package com.adi.czolg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication

public class CzolgApplication {

    public static void main(String[] args) {
        SpringApplication.run(CzolgApplication.class, args);
    }
}
//a.krause