package com.adi.czolg.model;

import lombok.Data;


@Data
public class Nation {
    private Long id;
    private String name;
    private String tank;
    private String flagBase64;
    private String address;
}
//a.krause