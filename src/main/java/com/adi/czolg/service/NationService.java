package com.adi.czolg.service;

import com.adi.czolg.model.Nation;
import org.springframework.stereotype.Service;

import java.util.List;
public interface NationService {
    List<Nation> findAllNation();
}
//a.krause