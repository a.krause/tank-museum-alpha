package com.adi.czolg.service;

import com.adi.czolg.model.Tank;
import com.google.common.io.ByteStreams;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class TankServiceImpl implements TankService {
    private List<Tank> tanksList;

    public TankServiceImpl (){
    tanksList=new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(NationServiceImpl.class.getResourceAsStream("/tanks/tanks.txt")));
       String t;
      try {
            while ((t = bufferedReader.readLine()) != null) {

                String[] t1 = t.split(";");
                String nation = t1[0];
                String tankName = t1[1];
                String tankFileName = t1[2];
                String information= t1[3];

               byte[] image = ByteStreams.toByteArray(TankServiceImpl.class.getResourceAsStream(tankFileName));
                 Tank tank=new Tank();
                tank.setName(tankName);
                tank.setNation(nation);
                tank.setInfo(information);
                String s=Base64.getEncoder().encodeToString(image);
                 tank.setTankBase64(s);
                 tank.setTankBase64("data:image/png;base64,"+ s);
               tanksList.add(tank);


        }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }


//    private final TankRespositories tankRespositories;
//
//    public TankServiceImpl(TankRespositories tankRespositories) {
//        this.tankRespositories = tankRespositories;
//    }
//
//    @Override
//    public Tank findTankbyId(Long id) {
//        return tankRespositories.getOne(id);
//    }
//
//    @Override
//    public List<Tank> findAllTank() {
//        return tankRespositories.findAll();
//    }

        }


//    @Override
//    public Tank findTankbyId(Long id) {
//        return null;
//    }

    @Override
    public List<Tank> findAllTank() {return tanksList;

    }
}
//a.krause