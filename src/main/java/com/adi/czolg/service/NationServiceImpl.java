package com.adi.czolg.service;

import com.adi.czolg.model.Nation;
import com.google.common.io.ByteStreams;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class NationServiceImpl implements NationService {
    private List<Nation> nationList;

    public NationServiceImpl() {
        nationList = new ArrayList<>();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(NationServiceImpl.class.getResourceAsStream("/nations/nations.txt")));
        String linia;
        try {
            while ((linia = bufferedReader.readLine()) != null) {
                String[] slowa = linia.split(";");
                String nationName = slowa[0];
                String nationFlagFileName = "/nations/" + slowa[1];
                String address=slowa[2];

                byte[] bytesImage = ByteStreams.toByteArray(NationServiceImpl.class.getResourceAsStream(nationFlagFileName));

                Nation nation = new Nation();
                nation.setName(nationName);
                nation.setAddress(address);

                String encoded = Base64.getEncoder().encodeToString(bytesImage);

                nation.setFlagBase64("data:image/png;base64," + encoded);
                nationList.add(nation);
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    @Override
    public List<Nation> findAllNation() {
        return nationList;
    }
//    private  final NationRespositories nationRespositories;
//    public NationServiceImpl(NationRespositories nationRespositories) {
//        this.nationRespositories = nationRespositories;
//    }
//    @Override
//    public Nation findNationById(Long id) {
//        return nationRespositories.getOne(id);
//    }
//    @Override
//    public List<Nation> findAllNation() {
//        return nationRespositories.findAll();
//    }

}
//a.krause