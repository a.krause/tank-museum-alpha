package com.adi.czolg.controller;

import com.adi.czolg.service.TankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/tanks")
public class TankController {
    private final TankService tankService;

    @Autowired
    public TankController(TankService tankService) {
        this.tankService = tankService;
    }

    @GetMapping(path = "/{nation}")
    public String test(@PathVariable String nation, Model model) {
        model.addAttribute("tanks",
                tankService.findAllTank()
                        .stream()
                        .filter(tank -> tank.getNation().equals(nation))
                        .collect(Collectors.toList()));
        return "tanks";
    }

}
// a.krause