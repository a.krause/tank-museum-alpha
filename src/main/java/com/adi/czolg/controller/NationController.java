package com.adi.czolg.controller;

import com.adi.czolg.service.NationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/nation")
public class NationController {
    private final NationService nationService;

    @Autowired
    public NationController(NationService nationService) {
        this.nationService = nationService;
    }

    @GetMapping(path = "")
    public String test(Model model) {
        model.addAttribute("nations", nationService.findAllNation());
        return "nations";
    }
}
//a.krause